package by.epam.task.report;

/**
 * Created by Pavel on 7/9/2016.
 *  not segergated
 */
public interface ReportPrinter {
    void printReport();
    int calculateReport();
}
